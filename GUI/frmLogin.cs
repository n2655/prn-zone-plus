﻿using System;
using System.Windows.Forms;
using BLL;
using DTO;

namespace GUI
{
    public partial class frmLogin : Form
    {
        private AccountBLL accountBLL;
        private static frmLogin instance;
        public static frmLogin GetForm
        {
            get => instance ?? (instance = new frmLogin());
        }

        public frmLogin()
        {
            InitializeComponent();
            accountBLL = new AccountBLL();

            BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            lbMessage.Text = "";
            if (txtUserUniqueId.Text == "" || txtPassword.Text == "")
            {
                lbMessage.Text = "Please fill all the fields!";
                return;
            }
            Account account = new Account(txtUserUniqueId.Text, txtPassword.Text);

            if (!accountBLL.CheckAccount(account)) lbMessage.Text = "We cannot recognize who you are. Please try again!";
        }

        private void llRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmRegister frmRegister = new frmRegister();
            frmRegister.Show();
            this.Hide();
        }
    }
}
