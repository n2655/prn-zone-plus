﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;

namespace GUI
{
    public partial class frmRegister : Form
    {
        private static frmRegister instance;
        private AccountBLL accountBLL;
        private bool isValidFirstName;
        private bool isValidLastName;
        private bool isValidAddress;
        private bool isValidPhoneNumber;
        private bool isValidEmail;
        private bool isValidPassword;
        private bool isValidRepassword;
        public static frmRegister GetForm
        {
            get => instance ?? (instance = new frmRegister());
        }

        public frmRegister()
        {
            InitializeComponent();
            accountBLL = new AccountBLL();
            isValidFirstName = false;
            isValidLastName = false;
            isValidAddress = false;
            isValidPhoneNumber = false;
            isValidEmail = false;
            isValidPassword = false;
            isValidRepassword = false;
        }

        private void sendValidMessage()
        {
            lbMessage.ForeColor = Color.MediumAquamarine;
            lbMessage.Text = "This field is valid!";
        }

        private void sendInvalidMessage(string message)
        {
            lbMessage.ForeColor = Color.LightCoral;
            lbMessage.Text = message;
        }

        private void llLogin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            frmLogin.GetForm.Show();
            Close();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            if (isValidFirstName &&
                isValidLastName &&
                isValidAddress &&
                isValidEmail &&
                isValidPassword &&
                isValidRepassword)
            {
                accountBLL.CreateAccount(new Account(
                    txtPhoneNumber.Text,
                    txtEmail.Text,
                    txtPassword.Text,
                    "GUEST"
                    ));
            }
        }

        private void txtPhoneNumber_TextChanged(object sender, EventArgs e)
        {
        }

        private void txtFirstName_TextChanged(object sender, EventArgs e)
        {
            if (txtFirstName.Text == "")
            {
                sendInvalidMessage("Your first name could not be empty!");
                isValidFirstName = false;
            }
            else
            {
                sendValidMessage();
                isValidFirstName = true;
            }
        }

        private void txtLastName_TextChange(object sender, EventArgs e)
        {
            if (txtLastName.Text == "")
            {
                sendInvalidMessage("Your last name could not be empty!");
                isValidFirstName = false;
            }
            else
            {
                sendValidMessage();
                isValidFirstName = true;
            }
        }

        private void txtAddress_TextChange(object sender, EventArgs e)
        {
            if (txtAddress.Text == "")
            {
                sendInvalidMessage("Your address could not be empty!");
                isValidFirstName = false;
            }
            else
            {
                sendValidMessage();
                isValidFirstName = true;
            }
        }

        private void txtPhoneNumber_TextChange(object sender, EventArgs e)
        {
            
        }

        private void txtEmail_TextChange(object sender, EventArgs e)
        {

        }

        private void txtPassword_TextChange(object sender, EventArgs e)
        {

        }

        private void txtConfirmPassworrd_TextChange(object sender, EventArgs e)
        {

        }
    }
}
