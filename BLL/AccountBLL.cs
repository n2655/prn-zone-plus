﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;

namespace BLL
{
    public class AccountBLL
    {
        private AccountDAL accountDAL = new DAL.AccountDAL();
        public bool CheckAccount(Account account) => accountDAL.CheckAccount(account);
        public int CreateAccount(Account account) => accountDAL.CreateAccount(account);
    }
}
