﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class DatabaseConnection
    {
        private SqlConnection connection;
        private string serverName;
        private string databaseName;
        private string userName;
        private string password;

        public DatabaseConnection()
        {
            serverName = "HUUWANGSPC";
            databaseName = "QUARANTINEZONE_DEMO";
            userName = "sa";
            password = "12345";
            connection = new SqlConnection();
            connection.ConnectionString = $"Data Source={serverName};" +
                                          $"Initial Catalog={databaseName};" +
                                          $"User ID={userName};" +
                                          $"Password={password}";
        }

        public SqlConnection GetConnection()
        {
            return connection;
        }

        public void OpenConnection()
        {
            if (connection.State == ConnectionState.Closed) connection.Open();
        }
        public void CloseConnection()
        {
            if (connection.State != ConnectionState.Closed) connection.Close();
        }
    }
}
