﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class AccountDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;

        public Account GetAccount(String phoneOrEmail)
        {
            query = "SELECT * \n" +
                           "FROM ACCOUNT \n" +
                           $"WHERE PHONE = {phoneOrEmail} OR EMAIL = {phoneOrEmail}";
            OpenConnection();

            command = new SqlCommand(query, GetConnection());
            result = command.ExecuteReader();

            while (result.Read())
            {
                return new Account(
                    result.GetString(0),
                    result.GetString(1),
                    result.GetString(2),
                    result.GetString(3));
            }
            CloseConnection();
            return null;
        }
        public bool CheckAccount(Account account)
        {
            query =
                $"SELECT * \n " +
                $"FROM ACCOUNT \n" +
                $"WHERE (PHONE = '{account.Phone}' OR EMAIL = '{account.Email}') AND PASSWORD = CONVERT(VARCHAR(32), HashBytes('MD5', '{account.Password}'), 2)";
            bool isTrue = false;

            OpenConnection();

            command = new SqlCommand(query, GetConnection());
            result = command.ExecuteReader();

            while (result.HasRows) {
                isTrue = true;
                break;
            }

            CloseConnection();
            return isTrue;
        }

        public int CreateAccount(Account account) 
        {
            query = "INSERT INTO ACCOUNT \n" +
            $"VALUES ({account.Phone}, {account.Email}, CONVERT(NVARCHAR(32), HashBytes('MD5', {account.Password}), 2), {account.Role})";
            OpenConnection();
            command= new SqlCommand(query, GetConnection());
            int rs = 0;

            try
            {
                rs = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            OpenConnection();
            return rs;
        }
        public int UpdatePassword(Account account) 
        {
            query = "UPDATE ACCOUNT \n" +
            $"SET PASSWORD = CONVERT(VARCHAR(32), HashBytes('MD5', {account.Password}), 2) \n" +
            $"WHERE EMAIL = {account.Email}";
            OpenConnection();
            command = new SqlCommand(query, GetConnection());
            int rs = 0;
            try
            {
                rs = command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            OpenConnection();
            return rs;
        }
        public bool IsExisted(string type, string ob) 
        {
            switch (type) 
            {
            case "email": query = $"EXEC IsExistedEmail {ob}"; break;
            case "phone": query = $"EXEC IsExistedPhone {ob}"; break;
            }
        bool isExisted = false;

        OpenConnection();
        command = new SqlCommand(query, GetConnection());
      
        result = command.ExecuteReader();

        if(result.Read()) isExisted = result.GetBoolean(0);

        CloseConnection();

            return isExisted;
        }

    public void DeleteAccount(String phoneOrEmail)
    {
        query = "DELETE FROM ACCOUNT\n" +
                $"WHERE PHONE = {phoneOrEmail} OR EMAIL = {phoneOrEmail}";

        OpenConnection();
        command=new SqlCommand(query, GetConnection());
        command.ExecuteNonQuery();
        CloseConnection();
        
    }



}
}
