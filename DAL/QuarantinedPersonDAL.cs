﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    internal class QuarantinedPersonDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;

        public QuarantinedPerson getQuarantinedPerson(Account account) 
        {
            query = "SELECT * \n" +
                    "FROM QUARANTINEDPERSON \n" +
                    "WHERE PHONE = @Phone";

            OpenConnection();

            command = new SqlCommand(query, Conn);

            command.Parameters.AddWithValue("Phone", account.Phone);
            result = command.ExecuteReader();

            while (result.Read()) {
                return new QuarantinedPerson(
                    result.GetString(1),
                    result.GetString(2),
                    result.GetString(3),
                    result.GetString(4),
                    result.GetString(5),
                    result.GetString(6),
                    result.GetString(7),
                    result.GetBoolean(8),
                    result.GetString(9),
                    result.GetString(10)
                );
            }
            CloseConnection();
            return null;
        }
    }
}
