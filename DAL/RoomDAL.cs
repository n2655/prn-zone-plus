﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class RoomDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;

        public Room GetRoomByRoomID(string roomID)
        {
            query = "SELECT * \n" +
                    "FROM ROOM JOIN ROOMTYPE ON ROOM.NAME = ROOMTYPE.NAME \n" +
                    $"WHERE ROOM.ID = '{roomID}'";

            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();

            while (result.Read())
            {
                RoomType roomtype = new RoomType(result.GetString(5), result.GetInt32(6), result.GetInt32(7));
                return new Room(result.GetString(1), roomtype, result.GetInt32(3), result.GetString(4));
            }

            CloseConnection();

            return null;
        }

        public int GetNumberOfDayInRoomByQuarantinedPersonID(string quarantinedPersonID)
        {
            query = $"SELECT dbo.RemainingDays('{quarantinedPersonID}')";
            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();

            while (result.Read())
            {
                return 21 - result.GetInt32(0);
            }

            CloseConnection();

            return -1;
        }

        public List<Room> GetRoomListByManagerID(string managerID)
        {
            query = "SELECT * FROM ROOM\n" +
                    "    JOIN MANAGER ON ROOM.MANAGERID = MANAGER.ID\n" +
                    "    JOIN ROOMTYPE ON ROOM.NAME = ROOMTYPE.NAME\n" +
                    $"WHERE ROOM.MANAGERID = '{managerID}'";
            List<Room> list = new List<Room>();

            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();

            while (result.Read())
            {
                RoomType roomtype = new RoomType(result.GetString(14), result.GetInt32(15), result.GetInt32(16));
                list.Add(new Room(result.GetString(1), roomtype, result.GetInt32(3), result.GetString(4)));
            }

            CloseConnection();
            return list;
        }

        public List<Room> GetAvailableRoomList()
        {
            query = "SELECT *,\n" +
                    "    CASE\n" +
                    "        WHEN NAME = 'Standard' AND NUMBEROFPEOPLE < 4 THEN 1\n" +
                    "        WHEN NAME = 'Luxury' AND NUMBEROFPEOPLE < 2 THEN 1\n" +
                    "        ELSE 0\n" +
                    "    END AS [AVAILABLE]\n" +
                    "FROM ROOM";
            List<Room> list = new List<Room>();
            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();

            while (result.Read())
            {
                if (result.GetInt32(5) != 0)
                {
                    list.Add(new Room(result.GetString(1)));
                }
            }

            CloseConnection();

            return list;
        }

        public List<Room> GetAvailableRoomListManagedByManager(string managerID)
        {
            query = "SELECT *,\n" +
                    "        CASE\n" +
                    "        WHEN NAME = 'Standard' AND NUMBEROFPEOPLE < 4 THEN 1\n" +
                    "        WHEN NAME = 'Luxury' AND NUMBEROFPEOPLE < 2 THEN 1\n" +
                    "        ELSE 0\n" +
                    "    END AS [AVAILABLE]\n" +
                    "    FROM ROOM\n" +
                    $"    WHERE MANAGERID = '{managerID}'";

            List<Room> list = new List<Room>();

            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();


            while (result.Read())
            {
                if (result.GetInt32(5) != 0)
                {
                    list.Add(new Room(result.GetString(1)));
                }
            }

            CloseConnection();

            return list;
        }
    }
}
