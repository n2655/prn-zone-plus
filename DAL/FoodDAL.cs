﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    internal class FoodDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;
        public List<Food> getFoodList()
        {
            query = "SELECT * \n" +
            "FROM FOOD";
            List<Food> list = new List<Food>();

        OpenConnection();
        command = new SqlCommand(query, Conn);
        result = command.ExecuteReader();

        while (result.Read()) {
            Food tempFood = new Food(result.GetString(1), result.GetString(2), result.GetInt32(3));
            list.Add(tempFood);
        }
        CloseConnection();
        return list;
}
    }
}
