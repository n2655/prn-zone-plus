﻿using DTO;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace DAL
{
    public class MealDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;

        public List<Meal> getMealListByQuarantinedPersonID(string id)
        {
            query = "SELECT * \n" +
                    "FROM FOODMANAGEMENT JOIN FOOD on FOODMANAGEMENT.FOODID = FOOD.ID \n" +
                    "WHERE QUARANTINEDPERSONID = @ID";
            OpenConnection();
            List<Meal> list = new List<Meal>();


            command = new SqlCommand(query, Conn);

            command.Parameters.AddWithValue("ID", id);
            result = command.ExecuteReader();


            while (result.Read())
            {

                Food tempFood = new Food(
                    result.GetString(5),
                    result.GetString(6),
                    result.GetInt32(7)
                );
                Meal tempMeal = new Meal(
                    result.GetString(0),
                    result.GetDateTime(1),
                    result.GetString(2),
                    tempFood);
                list.Add(tempMeal);
            }
            CloseConnection();
            return list;
        }

        public void updateMealByManager(Meal meal)
        {
            query = $"UPDATE FOODMANAGEMENT\n" +
            $"    SET FOODID = '{meal.Food}'\n" +
            $"WHERE QUARANTINEDPERSONID = '{meal.QuarantinedPersonID}' AND DATE = '{meal.Date.Date}' AND PERIOD = ?";

            OpenConnection();

            command = new SqlCommand(query, Conn);
            command.ExecuteNonQuery();

            CloseConnection();
        }


        public void createMealByManager(Meal meal)
        {
            query = "INSERT INTO FOODMANAGEMENT \n" +
            "VALUES (@QPId, @Date, @Period, @FoodID)";

            OpenConnection();
            command = new SqlCommand(query, Conn);

            command.Parameters.AddWithValue("QPId", meal.QuarantinedPersonID);
            command.Parameters.AddWithValue("Date", meal.Date.Date);
            command.Parameters.AddWithValue("QPId", meal.Period);
            command.Parameters.AddWithValue("QPId", meal.Food);

            command.ExecuteNonQuery();
            CloseConnection();
        }


    }
}
