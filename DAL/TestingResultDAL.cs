﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class TestingResultDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;

        public List<TestingResult> getListTestingResultByQuarantinedPersonID(string id) {
            query = "SELECT * \n" +
                    "FROM TESTINGRESULT \n" +
                    "WHERE QUARANTINEDPERSONID = ? \n" +
                    "ORDER BY DATE";
            List<TestingResult> list = new List<TestingResult>();

            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();


            while (result.Read())
            {
                
                TestingResult tempTestingResult = new TestingResult(
                    result.GetString(0),
                    result.GetString(1),
                    result.GetString(2)
                );
                list.Add(tempTestingResult);
            }

            

            return list;
        }
    }
}
