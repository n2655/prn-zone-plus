﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace DAL
{
    public class TestingCostDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;
        public List<TestingCost> GetTestingCost()
        {
            query = "SELECT * \n" + 
            "FROM TESTINGCOST";
            List<TestingCost> list = new List<TestingCost>();

            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();


                while (result.Read()) {
                TestingCost testingCost = new TestingCost(result.GetBoolean(0), result.GetInt32(1));
                list.Add(testingCost);
            }
            CloseConnection();

        return list;
    }

        public bool GetLatestResultByQuarantinedPersonID(string id){
            query = $"SELECT dbo.LatestResult('{id}')";

            OpenConnection();
            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();
            while (result.Read()) {
                return result.GetString(1).Equals("1");
            }
            CloseConnection();
           
            return false;
        }
        public int createTestingResult(TestingResult testingResult) 
        {
            query = "INSERT INTO TESTINGRESULT\n" +
                $"VALUES ('{testingResult.QuarantinedPersonId}', '{testingResult.Date.GetDateTimeFormats()}', '{testingResult.Result}')";

            OpenConnection();
            command = new SqlCommand(query, Conn);
            int rowEffect=command.ExecuteNonQuery();
            CloseConnection();
            return rowEffect;
        }

    public int updateTestingResult(TestingResult testingResult)
    {
        query = "UPDATE TESTINGRESULT\n" +
                $"SET RESULT = '{testingResult.Result}'\n" +
                $"WHERE QUARANTINEDPERSONID = '{testingResult.QuarantinedPersonId}' and DATE = '{testingResult.Date}'";

        OpenConnection();
        command = new SqlCommand(query, Conn);
        int rowEffect = command.ExecuteNonQuery();
        CloseConnection();
        return rowEffect;
        }

    }
}
