﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;

namespace DAL
{
    public class ManagerDAL : DatabaseConnection
    {
        private SqlCommand command;
        private SqlDataReader result;
        private string query;
        // Create function to get list of manager
        public List<Manager> GetManagerList()
        {
            query = "SELECT * \n" + "FROM MANAGER";
            // Create list to store manager
            List<Manager> list = new List<Manager>();
            OpenConnection();

            command = new SqlCommand(query, Conn);

            result = command.ExecuteReader();
            while (result.Read())
            {
                list.Add(new Manager(result.GetString(1),
                    result.GetString(2),
                    result.GetString(3),
                    result.GetString(4),
                    result.GetString(5),
                    result.GetString(6),
                    result.GetString(7),
                    result.GetString(8)));
            }
            // Close connection
            CloseConnection();
            return list;
        }

        // Create funtion to get manager by username
        public Manager GetManager(Account account)
        {
            query = "SELECT * \n" +
                    "FROM MANAGER \n" +
                    $"WHERE PHONE = '{account.Phone}'";

            OpenConnection();


            command = new SqlCommand(query, Conn);
            result = command.ExecuteReader();

            while (result.Read())
            {
                return new Manager(
                        result.GetString(1),
                        result.GetString(2),
                        result.GetString(3),
                        result.GetString(4),
                        result.GetString(5),
                        result.GetString(6),
                        result.GetString(7),
                        result.GetString(8)
                );
            }

            CloseConnection();

            return null;
        }

        public Manager getManagerByManagerID(string managerID)
        {
            query = "SELECT * FROM MANAGER\n" +
                    $"WHERE ID = '{managerID}'";

            OpenConnection();
            command = new SqlCommand(query, Conn);

            result = command.ExecuteReader();
            while (result.Read())
            {
                return new Manager(result.GetString(1),
                    result.GetString(2),
                    result.GetString(3),
                    result.GetString(4),
                    result.GetString(5),
                    result.GetString(6),
                    result.GetString(7),
                    result.GetString(8));
            }

            CloseConnection();
             
            return null;
        }


    }
}
