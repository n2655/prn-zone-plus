﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DatabaseConnection
    {
        private SqlConnection connection;
        private string serverName;
        private string databaseName;
        private string userName;
        private string password;

        public DatabaseConnection()
        {
            serverName = "eu-az-sql-serv1.database.windows.net";
            databaseName = "quarantinezone";
            userName = "zoneplus";
            password = "Nerdgang@123";
            connection = new SqlConnection();
            connection.ConnectionString = $"Data Source={serverName};" +
                                          $"Initial Catalog={databaseName};" +
                                          $"User ID={userName};" +
                                          $"Password={password}";
        }

        public SqlConnection GetConnection()
        {
            return connection;
        }

        public void OpenConnection()
        {
            if (connection.State == ConnectionState.Closed) connection.Open();
        }
        public void CloseConnection()
        {
            if (connection.State != ConnectionState.Closed) connection.Close();
        }
    }
}
