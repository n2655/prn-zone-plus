﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Room
    {
        public string Id { get; set; }
        public RoomType RoomType { get; set; }
        public int NumberOfPeople { get; set; }
        public string ManagerId { get; set; }

        public Room()
        {
            
        }
        public Room(string id) { this.Id = id; }
        public Room(string id, RoomType roomType, int numberOfPeople, string managerId)
        {
            Id = id;
            RoomType = roomType;
            NumberOfPeople = numberOfPeople;
            ManagerId = managerId;
        }
    }
}
