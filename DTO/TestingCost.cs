﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TestingCost
    {
        public bool InsuranceId { get; set; }
        public int Cost { get; set; }

        public TestingCost(bool insuranceId, int cost)
        {
            InsuranceId = insuranceId;
            Cost = cost;
        }
    }
}
