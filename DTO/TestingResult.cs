﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class TestingResult
    {
        public string QuarantinedPersonId { get; set; }
        public DateTime Date { get; set; }
        public string Result { get; set; }

        public TestingResult(string quarantinedPersonId, DateTime date, string result)
        {
            QuarantinedPersonId = quarantinedPersonId;
            Date = date;
            Result = result;
        }
        public TestingResult(String quarantinedPersonID, String date, String result) 
        {
            this.QuarantinedPersonId = quarantinedPersonID;
            setDate(date);
            this.Result = result;
    }

    public void setDate(string date) 
    {
        Date = DateTime.ParseExact(date, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
    }
    }
}
