﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class RoomType
    {
        public string Name { get; set; }
        public int MaximumPeople { get; set; }
        public int Cost { get; set; }

        public RoomType(string name, int maximumPeople, int cost)
        {
            Name = name;
            MaximumPeople = maximumPeople;
            Cost = cost;
        }
    }
}
