﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Manager
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public string AvatarURL { get; set; }

        public Manager()
        {
            
        }
        public Manager(string id)
        {
            this.Id = id;
        }
        public Manager(string id,
            string firstName,
            string lastName,
            string dateOfBirth,
            string address,
            string email,
            string phone,
            string avatarURL) 
        {
            this.Id = id;
        this.FirstName = firstName;
        this.LastName = lastName;
        setDateOfBirth(dateOfBirth);
            this.Address = address;
        this.Email = email;
        this.Phone = phone;
        this.AvatarURL = avatarURL;
    }
    public Manager(string id,
        string firstName,
        string lastName,
        string dateOfBirth,
        string address,
        string avatarURL) 
    {
       
        this.Id = id;
        this.FirstName = firstName;
        this.LastName = lastName;
        setDateOfBirth(dateOfBirth);
        this.Address = address;
        this.AvatarURL = avatarURL;
        }

    public void setDateOfBirth(string dateOfBirth) 
    {
         this.DateOfBirth= DateTime.ParseExact(dateOfBirth, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
    }
    }
}
