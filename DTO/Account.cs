﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Account
    {
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }

        public Account()
        {
            
        }

        public Account(string phoneOrEmail, string password)
        {
            Phone = phoneOrEmail;
            Email = phoneOrEmail;
            Password = password;
        }
    }
}
