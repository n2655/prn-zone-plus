﻿using System;

namespace DTO
{
    public class Meal
    {
        public string QuarantinedPersonID { set; get; }
        public DateTime Date { set; get; }
        public string Period { set; get; }
        public Food Food { set; get; }

        public Meal()
        {

        }

        public Meal(string quarantinedPersonId, DateTime date, string period, Food food)
        {
            QuarantinedPersonID = quarantinedPersonId;
            Date = date;
            Period = period;
        }

        public Meal(String quarantinedPersonID, String date, String period, String foodID)
        {
            QuarantinedPersonID = quarantinedPersonID;
            SetDate(date);
            Period = period;
            Food = new Food(foodID);
        }
        public Meal(String quarantinedPersonID, String date, String period, Food food)
        {
            QuarantinedPersonID = quarantinedPersonID;
            SetDate(date);
            Period = period;
            Food = food;
        }
        public void SetDate(string date)
        {
            //DateOfBirth = new DateTime((new SimpleDateFormat("yyyy-MM-dd")).parse(dateOfBirth).getTime());
            Date = DateTime.ParseExact(date, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        }



        internal static Meal CreateInstance(string quarantinedPersonId, DateTime date, string period, Food food)
        {
            return new Meal(quarantinedPersonId, date, period, food);
        }
    }
}
