﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Food
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }

        public Food()
        {
        }
        public Food(string id)
        {
            this.Id = id;
        }

        public Food(string id, string name, int price)
        {
            Id = id;
            Name = name;
            Price = price;
        }
    }
}
