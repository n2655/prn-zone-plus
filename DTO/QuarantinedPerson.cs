﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class QuarantinedPerson
    {
        public string Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public DateTime DateOfBirth { set; get; }
        public string Address { set; get; }
        public string Email { set; get; }
        public string Phone { set; get; }
        public bool InsuranceID { set; get; }
        public string RoomID { set; get; }
        public string AvatarURL { set; get; }
        public object P1 { get; }
        public object P2 { get; }
        public object P3 { get; }
        public object P4 { get; }
        public object P5 { get; }
        public object P6 { get; }
        public object P7 { get; }
        public object P8 { get; }
        public object P9 { get; }
        public object P10 { get; }

        public QuarantinedPerson(string id, string firstName, string lastName, DateTime dateOfBirth, string address, string email, string phone, bool insuranceId, string roomId, string avatarUrl)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dateOfBirth;
            Address = address;
            Email = email;
            Phone = phone;
            InsuranceID = insuranceId;
            RoomID = roomId;
            AvatarURL = avatarUrl;
        }
        public QuarantinedPerson(
            string id,
            string firstName,
            string lastName,
            string dateOfBirth,
            string address,
            string email,
            string phone,
            bool insuranceID,
            string roomID,
            string avatarURL)
        {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        SetDateOfBirth(dateOfBirth);
        Address = address;
        Email=email;
        Phone = phone;
        InsuranceID = insuranceID;
        RoomID = roomID;
        AvatarURL = avatarURL;
    }

    public QuarantinedPerson(
            string id,
            string firstName,
            string lastName,
            string dateOfBirth,
            string address,
            string email,
            string avatarURL) 
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        SetDateOfBirth(dateOfBirth);
        Address = address;
        Email=email;
        AvatarURL = avatarURL;
    }

    public QuarantinedPerson(
            string firstName,
            string lastName,
            string dateOfBirth,
            string address,
            string email,
            string phone,
            bool insuranceID,
            string roomID,
            string avatarURL) 
    {
        FirstName = firstName;
        LastName = lastName;
        SetDateOfBirth(dateOfBirth);
        Address = address;
        Email=email;
        Phone = phone;
        InsuranceID = insuranceID;
        RoomID = roomID;
        AvatarURL = avatarURL;
    }

    public QuarantinedPerson(string id,
                             string firstName,
                             string lastName,
                             string dateOfBirth,
                             string address,
                             bool insuranceID,
                             string roomID,
                             string avatarURL) 
    {
        Id = id;
        FirstName = firstName;
        LastName = lastName;
        SetDateOfBirth(dateOfBirth);
        Address = address;
        InsuranceID = insuranceID;
        RoomID = roomID;
        AvatarURL = avatarURL;
    }

        public QuarantinedPerson(object p1, object p2, object p3, object p4, object p5, object p6, object p7, object p8, object p9, object p10)
        {
            P1 = p1;
            P2 = p2;
            P3 = p3;
            P4 = p4;
            P5 = p5;
            P6 = p6;
            P7 = p7;
            P8 = p8;
            P9 = p9;
            P10 = p10;
        }

        public void SetDateOfBirth(string dateOfBirth) 
    {
        //DateOfBirth = new DateTime((new SimpleDateFormat("yyyy-MM-dd")).parse(dateOfBirth).getTime());
        DateOfBirth = DateTime.ParseExact(dateOfBirth, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
        }
}
}
